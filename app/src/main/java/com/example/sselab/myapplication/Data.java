package com.example.sselab.myapplication;

import java.util.ArrayList;

/**
 * Created by SSELAB on 2016-09-26.
 */
public class Data {

    static String[] items = {"전화걸기", "웹브라우져"};
    static String[] details = {
            "전화번호를 입력하세요. 입력양식은 \"tel:전화번호\"입니다.",
            "웹주소를 입력하세요. 입력양식은 \"http://웹주소\"입니다."
    };
}