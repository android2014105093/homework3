package com.example.sselab.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements ItemFragment.OnWordSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onWordSelected(int position) {
        DetailFragment detFrag = (DetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.detail_fragment);
        detFrag.updateDefinitionView(position);
    }

    public void onClickBtn(View v) {
        DetailFragment detFrag = (DetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.detail_fragment);

        EditText src = (EditText) detFrag.getView().findViewById(R.id.editText);

        Intent intent = null;
        switch(detFrag.mCurrentPosition) {
            case 0:
                intent = new Intent(Intent.ACTION_DIAL, Uri.parse(src.getText().toString()));
                break;
            case 1:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(src.getText().toString()));
                break;
        }
        startActivity(intent);
    }
}